﻿#include "Background.h"

Background::Background()
{
	m_parallaxStrength = 0.5;

	m_Top_Scale = 1.0;
	m_BottomScale = 1.0;

	m_focusStrength = 0.5;
	m_focusPoint = Vector2();

	m_pTopTexture = nullptr;
	m_pBottomTexture = nullptr;
}

void Background::Draw(SpriteBatch* pSpriteBatch, float opacity)
{
	Vector2 topDrawPosition = GetTopTextureDrawPosition();
	Vector2 bottomDrawPosition = topDrawPosition * m_parallaxStrength;

	if (m_pBottomTexture)
	{
		DrawTiled(pSpriteBatch, m_pBottomTexture, bottomDrawPosition, m_BottomScale, opacity);;;
	}
	if (m_pTopTexture)
	{
		DrawTiled(pSpriteBatch, m_pTopTexture, topDrawPosition, m_Top_Scale, opacity);
	}
}

int max(int first, int second)
{
	return first > second ? first : second;
}

/*
 basePosition controls where the texture is drawn on the screen. It is unbounded,
 so we constrain it to the bound shown below, then draw enough tiles starting at
 the basePosition to fill the screen.

                       | basePosition bounding box
                       V
                ╔══════════╗
                ║          ║
 example        ║          ║
 basePosition ──╫─>●───────╫──┬──────────┬──────────┬──────────┬──────────┬──────────┐
 after          ║  │       ║  │          │          │          │          │          │ first
 bounding       ╚══╪═══════╬══╪══════════╪══════════╪══════════╪══════════╪═══╗      │ row
                   │       ║  │          │          │          │          │   ║      │ of
                   │       ║  │          │          │          │          │   ║      │ tiles
                   ├───────╫──┼──────────┴──────────┴──────────┴──────────┴───╫──────┘
                   │       ║  │                                               ║
                   │       ║  │                                               ║
                   │       ║  │                                               ║
                   │       ║  │                  display                      ║
                   ├───────╫──┤                                               ║
                   │       ║  │                                               ║
                   │       ║  │                                               ║
                   │       ║  │                                               ║
                   │       ║  │                                               ║
                   ├───────╫──┤                                               ║
                   │       ╚══╪═══════════════════════════════════════════════╝
                   │          │
                   │          │
                   │          │
                   └──────────┘
				   first column
				   of tiles
*/
void DrawTiled(SpriteBatch* pSpriteBatch, Texture* pTexture, Vector2 basePosition, float scale, float opacity)
{
	int screenWidth = Game::GetScreenWidth();
	int screenHeight = Game::GetScreenHeight();

	int textureWidth = pTexture->GetWidth();
	int textureHeight = pTexture->GetHeight();
	// actual height and width the texture will be drawn as
	float scaledTextureWidth = scale * textureHeight;
	float scaledTextureHeight = scale * textureWidth;

	// the number of tiles required to fill the screen + 1 because the first tile
	// in each row and column can be up to completely off the screen
	int horizontalTileCount = ceil((float)screenWidth / textureHeight / scale) + 1;
	int verticalTileCount = ceil((float)screenHeight / textureWidth / scale) + 1;


	// bound basePosition horizontally
	float horizontalOffset = fmod(basePosition.X, scaledTextureWidth);
	horizontalOffset += horizontalOffset < 0.0 ? scaledTextureWidth : 0.0;
	basePosition.X = (float)-scaledTextureWidth + horizontalOffset;

	// bound basePosition vertically
	float verticalOffset = fmod(basePosition.Y, scaledTextureHeight);
	verticalOffset += verticalOffset < 0.0 ? scaledTextureHeight : 0.0;
	basePosition.Y = (float)-scaledTextureHeight + verticalOffset;


	for (int horizontalTile = 0; horizontalTile < horizontalTileCount; horizontalTile++)
	{
		for (int verticalTile = 0; verticalTile < verticalTileCount; verticalTile++)
		{
			Vector2 drawPosition = basePosition;
			drawPosition.X += horizontalTile * scaledTextureWidth;
			drawPosition.Y += verticalTile * scaledTextureHeight;
			pSpriteBatch->Draw(
				pTexture,
				drawPosition,
				Color::White * opacity,
				Vector2::ZERO,
				Vector2(scale, scale)
			);
		}
	}
}
