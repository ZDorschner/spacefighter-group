#pragma once
#include "Background.h"
#include "MenuBackgroundNoise.h"

const int RANDOM_HISTORY_LENGTH = 200;

// A background for menus, which has random motion
class MenuBackground : public Background
{
public:
	// should be called in menu update; focusPoint will be ignored
	virtual void Update(const GameTime* pGameTime, Vector2 focusPoint = Vector2::ZERO);

	virtual Vector2 GetTopTextureDrawPosition();

	// sets how quickly the background should move
	void SetMotionSpeed(float speed) { m_motionSpeed = speed; }

	// sets how far the background should move
	void SetMotionScale(float scale) { m_motionScale = scale; }

protected:
	float m_motionSpeed = 1.0;
	float m_motionScale = 10.0;
};

