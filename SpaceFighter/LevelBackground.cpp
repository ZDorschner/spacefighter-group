#include "LevelBackground.h"

LevelBackground::LevelBackground()
{
	m_scrollSpeed = 1.0;
	m_scrollPosition = 0.0;
}

void LevelBackground::Update(const GameTime* pGameTime, Vector2 focusPoint)
{
	m_scrollPosition += m_scrollSpeed * pGameTime->GetTimeElapsed();
	m_focusPoint = focusPoint;
}

Vector2 LevelBackground::GetTopTextureDrawPosition()
{
	Vector2 position = -m_focusPoint;
	position.Y += m_scrollPosition;
	return position;
}
