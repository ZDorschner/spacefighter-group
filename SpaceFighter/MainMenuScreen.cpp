
#include <string>
#include "MainMenuScreen.h"
#include "GameplayScreen.h"

// Callback Functions
void OnStartGameSelect(MenuScreen *pScreen)
{
	pScreen->UnloadContent();
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen());
}

void OnQuitSelect(MenuScreen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	pMainMenuScreen->SetQuitFlag();
	pMainMenuScreen->Exit();
}

void OnScreenRemove(Screen *pScreen)
{
	MainMenuScreen *pMainMenuScreen = (MainMenuScreen *)pScreen;
	if (pMainMenuScreen->IsQuittingGame()) pScreen->GetGame()->Quit();
}



MainMenuScreen::MainMenuScreen()
{
	m_pTexture = nullptr;
	m_pMenuMusic = nullptr;
	m_pMenuSelectSound = nullptr;
	m_pMenuEnterSound = nullptr;

	SetRemoveCallback(OnScreenRemove);

	SetTransitionInTime(1.0f);
	SetTransitionOutTime(0.5f);

	Show(); // Show the screen
}

void MainMenuScreen::LoadContent(ResourceManager *pResourceManager)
{
	// Logo
	m_pTexture = pResourceManager->Load<Texture>("Textures\\Logo.png");
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 150;

	// Audio
	m_pMenuMusic = pResourceManager->Load<Audio>("Sounds\\mainmenu.ogg");
	m_pMenuSelectSound = pResourceManager->Load<Audio>("Sounds\\menuselect.ogg");
	m_pMenuEnterSound = pResourceManager->Load<Audio>("Sounds\\menuenter.ogg");

	// Play Background Music
	m_pMenuMusic->PlaySampleInstance(ALLEGRO_PLAYMODE_LOOP);

	// Create the menu items
	const int COUNT = 2;
	MenuItem *pItem;
	Font::SetLoadSize(20, true);
	Font *pFont = pResourceManager->Load<Font>("Fonts\\Ethnocentric.ttf");

	SetDisplayCount(COUNT);

	enum Items { START_GAME, QUIT };
	std::string text[COUNT] = { "Start Game", "Quit" };

	for (int i = 0; i < COUNT; i++)
	{
		pItem = new MenuItem(text[i]);
		pItem->SetPosition(Vector2(100, 100 + 50 * i));
		pItem->SetFont(pFont);
		pItem->SetColor(Color::Blue);
		pItem->SetSelected(i == 0);
		AddMenuItem(pItem);
	}

	GetMenuItem(START_GAME)->SetSelectCallback(OnStartGameSelect);
	GetMenuItem(QUIT)->SetSelectCallback(OnQuitSelect);

	m_background.SetTopTexture(pResourceManager->Load<Texture>("Textures\\background.png"));
	m_background.SetBottomTexture(pResourceManager->Load<Texture>("Textures\\menu_background_lower.png"));
	m_background.SetFocusStrength(250.0);
	m_background.SetParallaxStrength(0.7);
	m_background.SetBottomScale(2.5);
	m_background.SetMotionScale(20.0);
	m_background.SetMotionSpeed(0.7);
}

void MainMenuScreen::HandleInput(const InputState* pInput) {
	// Load all regular input handlers
	MenuScreen::HandleInput(pInput);
	int8_t playerIndexOut; // Gamepad IndexOut

	// Plays a sound when up/down on keyboard or gamepad are pressed
	if (pInput->IsNewKeyPress(Key::UP) || pInput->IsNewButtonPress(Button::DPAD_UP, playerIndexOut)
		|| pInput->IsNewKeyPress(Key::DOWN) || pInput->IsNewButtonPress(Button::DPAD_DOWN, playerIndexOut))
		if (m_pMenuSelectSound) m_pMenuSelectSound->PlaySampleInstance();

	// Plays a soound when enter or A on gamepad is pressed
	if (pInput->IsNewKeyPress(Key::ENTER) ||
		pInput->IsNewButtonPress(Button::A, playerIndexOut))
		if (m_pMenuEnterSound) m_pMenuEnterSound->PlaySampleInstance();
}
void MainMenuScreen::Update(const GameTime *pGameTime)
{
	MenuItem *pItem;

	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());

		if (pItem->IsSelected()) pItem->SetColor(Color::White);
		else pItem->SetColor(Color::Blue);
	}

	MenuScreen::Update(pGameTime);

	m_background.Update(pGameTime);
}

void MainMenuScreen::Draw(SpriteBatch *pSpriteBatch)
{
	pSpriteBatch->Begin();
	m_background.Draw(pSpriteBatch, GetAlpha());
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	pSpriteBatch->End();

	MenuScreen::Draw(pSpriteBatch);
}
void MainMenuScreen::UnloadContent() {
	// Stop Background Music
	if (m_pMenuMusic) m_pMenuMusic->StopSampleInstance();
}