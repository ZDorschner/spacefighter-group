
#pragma once

#include "Weapon.h"
#include "TriggerType.h"

class Blaster : public Weapon
{

public:

	Blaster(const bool isActive) : Weapon(isActive)
	{
		m_cooldown = 0;
		m_cooldownSeconds = 0.35;
	}

	virtual ~Blaster() { }

	virtual void Update(const GameTime *pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void Fire(TriggerType triggerType)
	{
		if (IsActive() && CanFire())
		{
			if (triggerType.Contains(GetTriggerType()))
			{
				Projectile *pProjectile = GetProjectile();
				if (triggerType.Contains(triggerType.PRIMARY)) // checks to see whos firing
				{
					pProjectile->Activate(GetPosition(), true);
					m_cooldown = m_cooldownSeconds;
				}
				else if (triggerType.Contains(triggerType.SECONDARY))
				{
					pProjectile->Activate(GetPosition(), false);
					m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}


private:

	float m_cooldown;
	float m_cooldownSeconds;

};