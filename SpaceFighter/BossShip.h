
#pragma once

#include "EnemyShip.h"

class BossShip : public EnemyShip
{

public:

	BossShip();
	virtual ~BossShip() { }

	void SetTexture(Texture* pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime);

	virtual void LoadContent(ResourceManager* pResourceManager);

	virtual void Draw(SpriteBatch* pSpriteBatch);

	virtual void ConfineToScreen(const bool isConfined = true) { m_isConfinedToScreen = isConfined; }


protected:

	virtual float GetResponsiveness() const { return m_responsiveness; }

private:

	Texture* m_pTexture;
	bool m_isConfinedToScreen;
	Vector2 m_desiredDirection;
	Vector2 m_velocity;
	float m_responsiveness;
	bool m_side;

};