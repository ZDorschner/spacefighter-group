#include "MenuBackgroundNoise.h"

static float orgBlenderNoise(float x, float y, float z)
{
	float cn1, cn2, cn3, cn4, cn5, cn6, i;
	const float* h;
	float fx, fy, fz, ox, oy, oz, jx, jy, jz;
	float n = 0.5;
	int ix, iy, iz, b00, b01, b10, b11, b20, b21;

	fx = floor(x);
	fy = floor(y);
	fz = floor(z);

	ox = x - fx;
	oy = y - fy;
	oz = z - fz;

	ix = (int)fx;
	iy = (int)fy;
	iz = (int)fz;

	jx = ox - 1;
	jy = oy - 1;
	jz = oz - 1;

	cn1 = ox * ox;
	cn2 = oy * oy;
	cn3 = oz * oz;
	cn4 = jx * jx;
	cn5 = jy * jy;
	cn6 = jz * jz;

	cn1 = 1.0f - 3.0f * cn1 + 2.0f * cn1 * ox;
	cn2 = 1.0f - 3.0f * cn2 + 2.0f * cn2 * oy;
	cn3 = 1.0f - 3.0f * cn3 + 2.0f * cn3 * oz;
	cn4 = 1.0f - 3.0f * cn4 - 2.0f * cn4 * jx;
	cn5 = 1.0f - 3.0f * cn5 - 2.0f * cn5 * jy;
	cn6 = 1.0f - 3.0f * cn6 - 2.0f * cn6 * jz;

	b00 = hash[hash[ix & 255] + (iy & 255)];
	b10 = hash[hash[(ix + 1) & 255] + (iy & 255)];
	b01 = hash[hash[ix & 255] + ((iy + 1) & 255)];
	b11 = hash[hash[(ix + 1) & 255] + ((iy + 1) & 255)];

	b20 = iz & 255;
	b21 = (iz + 1) & 255;

	/* 0 */
	i = (cn1 * cn2 * cn3);
	h = hashvectf + 3 * hash[b20 + b00];
	n += i * (h[0] * ox + h[1] * oy + h[2] * oz);
	/* 1 */
	i = (cn1 * cn2 * cn6);
	h = hashvectf + 3 * hash[b21 + b00];
	n += i * (h[0] * ox + h[1] * oy + h[2] * jz);
	/* 2 */
	i = (cn1 * cn5 * cn3);
	h = hashvectf + 3 * hash[b20 + b01];
	n += i * (h[0] * ox + h[1] * jy + h[2] * oz);
	/* 3 */
	i = (cn1 * cn5 * cn6);
	h = hashvectf + 3 * hash[b21 + b01];
	n += i * (h[0] * ox + h[1] * jy + h[2] * jz);
	/* 4 */
	i = cn4 * cn2 * cn3;
	h = hashvectf + 3 * hash[b20 + b10];
	n += i * (h[0] * jx + h[1] * oy + h[2] * oz);
	/* 5 */
	i = cn4 * cn2 * cn6;
	h = hashvectf + 3 * hash[b21 + b10];
	n += i * (h[0] * jx + h[1] * oy + h[2] * jz);
	/* 6 */
	i = cn4 * cn5 * cn3;
	h = hashvectf + 3 * hash[b20 + b11];
	n += i * (h[0] * jx + h[1] * jy + h[2] * oz);
	/* 7 */
	i = (cn4 * cn5 * cn6);
	h = hashvectf + 3 * hash[b21 + b11];
	n += i * (h[0] * jx + h[1] * jy + h[2] * jz);

	if (n < 0.0f) {
		n = 0.0f;
	}
	else if (n > 1.0f) {
		n = 1.0f;
	}
	return n;
}

float BLI_hnoise(float noisesize, float x, float y, float z)
{
	if (noisesize == 0.0f) {
		return 0.0f;
	}
	x = (1.0f + x) / noisesize;
	y = (1.0f + y) / noisesize;
	z = (1.0f + z) / noisesize;
	return orgBlenderNoise(x, y, z);
}

float BLI_turbulence(float noisesize, float x, float y, float z, int nr)
{
	float s, d = 0.5, div = 1.0;

	s = BLI_hnoise(noisesize, x, y, z);

	while (nr > 0) {

		s += d * BLI_hnoise(noisesize * d, x, y, z);
		div += d;
		d *= 0.5f;

		nr--;
	}
	return s / div;
}

