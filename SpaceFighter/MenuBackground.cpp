#include "MenuBackground.h"

void MenuBackground::Update(const GameTime* pGameTime, Vector2 focusPoint)
{
	// depth 0 gives smoothest curve
	// phase 0 and 20 are arbritrarily chosen to give different curves
	float positionX = (BLI_turbulence(7.0, pGameTime->GetTotalTime() * m_motionSpeed, 0.0, 0.1, 0) - 0.5) * m_motionScale;
	float positionY = (BLI_turbulence(7.0, pGameTime->GetTotalTime() * m_motionSpeed, 20.0, 0.1, 0) - 0.5) * m_motionScale;

	// the position is added instead of set, so the focus point has unbounded range / variance
	m_focusPoint += Vector2(positionX, positionY);
}

Vector2 MenuBackground::GetTopTextureDrawPosition()
{
	return m_focusPoint;
}
