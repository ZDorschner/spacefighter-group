#pragma once

#include "KatanaEngine.h"
#include "MenuBackground.h"

using namespace KatanaEngine;

class MainMenuScreen : public MenuScreen
{

public:

	MainMenuScreen();

	virtual ~MainMenuScreen() { }

	virtual void LoadContent(ResourceManager *pResourceManager);
	
	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);

	virtual void HandleInput(const InputState* pInput);

	virtual void UnloadContent();

	virtual void SetQuitFlag() { m_isQuittingGame = true; }

	virtual bool IsQuittingGame() { return m_isQuittingGame; }


private:
	
	Texture *m_pTexture;

	Audio* m_pMenuMusic;

	Audio* m_pMenuSelectSound;

	Audio* m_pMenuEnterSound;

	Vector2 m_texturePosition;

	bool m_isQuittingGame = false;

	MenuBackground m_background;
};