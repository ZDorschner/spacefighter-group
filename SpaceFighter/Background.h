#pragma once

#include <string>
#include <math.h>
#include "KatanaEngine.h"
using namespace KatanaEngine;

void DrawTiled(SpriteBatch* pSpriteBatch, Texture* pTexture, Vector2 drawPosition, float scale, float opacity);

class Background
{
public:
	Background();
	virtual ~Background() {}

	void SetTopTexture(Texture* pTexture) { m_pTopTexture = pTexture; }
	void SetBottomTexture(Texture* pTexture) { m_pBottomTexture = pTexture; }

	virtual void Update(const GameTime* pGameTime, Vector2 focusPoint = Vector2::ZERO) = 0;

	virtual void Draw(SpriteBatch* pSpriteBatch, float opacity = 1.0);

	// allows customizing draw position without overriding draw
	virtual Vector2 GetTopTextureDrawPosition() = 0;

	// sets the number of units the bottom texture is moved
	// per unit that the top texture is moved
	void SetParallaxStrength(float strength) { m_parallaxStrength = strength; }

	// sets the number of units that the top texture is moved
	// per unit that the focus is moved
	void SetFocusStrength(float strength) { m_focusStrength = strength; }

	// sets the scale of the top texture
	void SetTopScale(float scale) { if(scale > 0.0) m_Top_Scale = scale; }

	// sets the scale of the bottom texture
	void SetBottomScale(float scale) { if (scale > 0.0) m_BottomScale = scale; }

protected:
	float m_parallaxStrength;

	float m_Top_Scale;
	float m_BottomScale;

	float m_focusStrength;
	Vector2 m_focusPoint;

	Texture* m_pTopTexture;
	Texture* m_pBottomTexture;
};
