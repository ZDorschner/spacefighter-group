
#include "BossShip.h"


BossShip::BossShip()
{
	SetSpeed(300);
	SetMaxHitPoints(5);
	SetCollisionRadius(40);
}

void BossShip::LoadContent(ResourceManager* pResourceManager)
{
	ConfineToScreen();

	m_pTexture = pResourceManager->Load<Texture>("Textures\\BossShip.png");

	SetPosition(Game::GetScreenCenter() + Vector2::UNIT_Y * 500);
}
void BossShip::Update(const GameTime* pGameTime)
{
	if (IsActive())
	{
		const int PADDING = 4; // keep the ship 4 pixels from the edge of the screen
		const int TOP = PADDING;
		const int LEFT = PADDING;
		const int RIGHT = Game::GetScreenWidth() - PADDING;
		const int BOTTOM = Game::GetScreenHeight() - PADDING;
		
		float x = GetIndex();

		FireWeapons(TriggerType::SECONDARY); // constantly shoots
		if (m_side == true) {
		x *= (GetSpeed() * .01f) * pGameTime->GetTimeElapsed() * 1.0f;
		TranslatePosition(x, 0); // keeps the y axis untouched
		}
		else
		{
		x *= -((GetSpeed() * .01f) * pGameTime->GetTimeElapsed() * 1.0f);
		TranslatePosition(x, 0); // keeps the y axis untouched
		}
		

			Vector2* pPosition = &GetPosition(); // current position (middle of the ship)
			if (pPosition->X - GetHalfDimensions().X < LEFT) // are we past the left edge?
			{
				// move the ship to the left edge of the screen (keep Y the same)
				SetPosition(LEFT + GetHalfDimensions().X, pPosition->Y);
					m_side = true;
			}
			if (pPosition->X + GetHalfDimensions().X > RIGHT) // right edge?
			{
				SetPosition(RIGHT - GetHalfDimensions().X, pPosition->Y); 
				m_side = false;
					
			}
			
		
	}

	EnemyShip::Update(pGameTime);
}


void BossShip::Draw(SpriteBatch* pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, 0, 1);
	}
}