

#include "Level01.h"
#include "BioEnemyShip.h"
#include "BossShip.h"
#include "Blaster.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");
	Texture *pBossTexture = pResourceManager->Load<Texture>("Textures\\BossShip.png");
	Audio* pExplosionSound = pResourceManager->Load<Audio>("Sounds\\explosion.ogg");
	Audio* pBossExplosionSound = pResourceManager->Load<Audio>("Sounds\\bossexplosion.ogg");
	Audio* pHitSound = pResourceManager->Load<Audio>("Sounds\\hit.ogg");

	//Set Up Level Audio
	Audio *pLevelMusic = pResourceManager->Load<Audio>("Sounds\\levelmusic.ogg");

	// Play level music if it successfully loaded.
	if (pLevelMusic) pLevelMusic->PlaySampleInstance(ALLEGRO_PLAYMODE_LOOP);

	const int COUNT = 21;
	const int BOSS = 1;

	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};

	float delay = 3.0; // start delay
	Vector2 position;
	for (int i = 0; i < COUNT; i++)
	{
		delay += delays[i];
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetExplosionSound(pExplosionSound);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

		// Spawns the boss in
		position.Set(Game::GetScreenWidth(), pBossTexture->GetCenter().Y + 50);
		BossShip* pBoss = new BossShip();
		
		pBoss->SetTexture(pBossTexture);
		pBoss->SetCurrentLevel(this);
		pBoss->Initialize(position, delay);
		pBoss->SetExplosionSound(pBossExplosionSound);
		pBoss->SetHitSound(pHitSound);

		// Setup Boss ship
		Blaster* pBossBlaster = new Blaster(true);
		pBossBlaster->SetProjectilePool(&m_bossprojectiles);
		pBoss->AttachWeapon(pBossBlaster, Vector2::UNIT_Y * 50);

		for (int i = 0; i < 100; i++)
		{
			Projectile* pProjectile = new Projectile();
			m_bossprojectiles.push_back(pProjectile);
			AddGameObject(pProjectile);
		}
		AddGameObject(pBoss);
		
	
	
	Texture* topTexture = pResourceManager->Load<Texture>("Textures\\background.png");
	Texture* bottomTexture = pResourceManager->Load<Texture>("Textures\\lower_background.png");
	m_background.SetTopTexture(topTexture);
	m_background.SetBottomTexture(bottomTexture);
	m_background.SetScrollSpeed(100.0);
	m_background.SetFocusStrength(0.5);
	m_background.SetParallaxStrength(0.5);
	m_background.SetBottomScale(2.0);

	Level::LoadContent(pResourceManager);
}

