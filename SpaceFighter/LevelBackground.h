#pragma once
#include "Background.h"

// A background (for levels) that responds to where the player ship is positioned
class LevelBackground : public Background
{
public:
	LevelBackground();

	// should be called in level update; a focusPoint must be passed for LevelBackground
	// to behave correctly
	virtual void Update(const GameTime* pGameTime, Vector2 focusPoint = Vector2::ZERO);

	virtual Vector2 GetTopTextureDrawPosition();

	// sets the number of units per second that the top texture is scrolled
	void SetScrollSpeed(float speed) { m_scrollSpeed = speed; }

protected:
	float m_scrollSpeed;
	float m_scrollPosition;
};

