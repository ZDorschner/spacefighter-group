#pragma once

#include "Level.h"
#include "BossShip.h"

class Level01 :	public Level
{

public:
	
	Level01() { }

	virtual ~Level01() { }

	virtual void LoadContent(ResourceManager *pResourceManager);

	virtual void UnloadContent() { }

private:

	BossShip* m_pBossShip;
	std::vector<Projectile*> m_bossprojectiles;
};

