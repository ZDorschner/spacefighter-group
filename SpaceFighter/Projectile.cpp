
#include "Projectile.h"

Texture *Projectile::s_pTexture = nullptr;
Audio* Projectile::s_pSound = nullptr;

Projectile::Projectile()
{
	SetSpeed(500);
	SetDamage(1);
	SetDirection(-Vector2::UNIT_Y);
	SetCollisionRadius(9);

	m_drawnByLevel = true;
}

void Projectile::Update(const GameTime *pGameTime) // depending on who shoots it depends on how the bullets move
{
	if (IsActive())
	{
		Vector2 translation = m_direction * m_speed * pGameTime->GetTimeElapsed();
		
		if (WasShotByPlayer() == true)
			TranslatePosition(translation);
		else
			TranslatePosition(-translation);

		Vector2 position = GetPosition();
		Vector2 size = s_pTexture->GetSize();

		// Is the projectile off the screen?
		if (position.Y < -size.Y) Deactivate();
		else if (position.X < -size.X) Deactivate();
		else if (position.Y > Game::GetScreenHeight() + size.Y) Deactivate();
		else if (position.X > Game::GetScreenWidth() + size.X) Deactivate();
	}

	GameObject::Update(pGameTime);
}

void Projectile::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		if(WasShotByPlayer() == true) // sets different bullet types
		pSpriteBatch->Draw(s_pTexture, GetPosition(), Color::White, s_pTexture->GetCenter());
		else
		pSpriteBatch->Draw(s_pTexture, GetPosition(), Color::Red, s_pTexture->GetCenter());
	}
}

void Projectile::Activate(const Vector2 &position, bool wasShotByPlayer)
{
	m_wasShotByPlayer = wasShotByPlayer;
	SetPosition(position);
	s_pSound->PlaySampleInstance();

	GameObject::Activate();
}

std::string Projectile::ToString() const
{
	return ((WasShotByPlayer()) ? "Player " : "Enemy ") + GetProjectileTypeString(); // checks to see if the player or the boss shoots it
}

CollisionType Projectile::GetCollisionType() const
{
	CollisionType shipType = WasShotByPlayer() ? CollisionType::PLAYER : CollisionType::ENEMY; 
	return (shipType | GetProjectileType());
}