﻿
/*
	 ██╗  ██╗  █████╗  ████████╗  █████╗  ███╗   ██╗  █████╗
	 ██║ ██╔╝ ██╔══██╗ ╚══██╔══╝ ██╔══██╗ ████╗  ██║ ██╔══██╗
	 █████╔╝  ███████║    ██║    ███████║ ██╔██╗ ██║ ███████║
	 ██╔═██╗  ██╔══██║    ██║    ██╔══██║ ██║╚██╗██║ ██╔══██║
	 ██║  ██╗ ██║  ██║    ██║    ██║  ██║ ██║ ╚████║ ██║  ██║
	 ╚═╝  ╚═╝ ╚═╝  ╚═╝/\  ╚═╝    ╚═╝  ╚═╝ ╚═╝  ╚═══╝ ╚═╝  ╚═╝
   /vvvvvvvvvvvvvvvvvvv \=========================================,
   `^^^^^^^^^^^^^^^^^^^ /---------------------------------------"
		Katana Engine \/ © 2012 - Shuriken Studios LLC
				http://www.shurikenstudios.com
*/

#pragma once

namespace KatanaEngine
{
	/** @brief Represents an audio file. */
	class Audio : public Resource
	{

	public:

		Audio() { }
		virtual ~Audio()
		{
			al_destroy_sample(m_psample);
			al_destroy_sample_instance(m_psampleInstance);
		}


		/** @brief Load the desired audio file into memory.
			@param path The path to the desired audio file. Only Accepts Extensions: .wav, .ogg, .flac, .it, .mod, .s3m, & .xm
			@param pManager A pointer to the ResourceManager that will manage the audio file.
			@return Returns true if the audio file was loaded, false otherwise. */
		virtual bool Load(const std::string& path, ResourceManager* pManager);


		/** @brief Used to determine if the audio is cloneable.
			@return Returns true if the audio is clonable, false otherwise.
			@remark Audio should usually not be cloneable. */
		virtual bool IsCloneable() const { return false; }

		/** @Create an Instance of the current audio sample loaded into memory and play it
			@param mode The mode in which to play the sample (ALLEGRO_PLAYMODE_ONCE, ALLEGRO_PLAYMODE_LOOP).*/
		virtual void PlaySampleInstance(ALLEGRO_PLAYMODE mode = ALLEGRO_PLAYMODE_ONCE);

		//** Stop playing the current sample audi instance */
		virtual void StopSampleInstance();

	private:

		static bool s_alAddonInitialized;

		ALLEGRO_SAMPLE* m_psample = NULL;
		ALLEGRO_SAMPLE_INSTANCE* m_psampleInstance = NULL;

	};
}