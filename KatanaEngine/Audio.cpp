﻿
/*
	 ██╗  ██╗  █████╗  ████████╗  █████╗  ███╗   ██╗  █████╗
	 ██║ ██╔╝ ██╔══██╗ ╚══██╔══╝ ██╔══██╗ ████╗  ██║ ██╔══██╗
	 █████╔╝  ███████║    ██║    ███████║ ██╔██╗ ██║ ███████║
	 ██╔═██╗  ██╔══██║    ██║    ██╔══██║ ██║╚██╗██║ ██╔══██║
	 ██║  ██╗ ██║  ██║    ██║    ██║  ██║ ██║ ╚████║ ██║  ██║
	 ╚═╝  ╚═╝ ╚═╝  ╚═╝/\  ╚═╝    ╚═╝  ╚═╝ ╚═╝  ╚═══╝ ╚═╝  ╚═╝
   /vvvvvvvvvvvvvvvvvvv \=========================================,
   `^^^^^^^^^^^^^^^^^^^ /---------------------------------------"
		Katana Engine \/ © 2012 - Shuriken Studios LLC
				http://www.shurikenstudios.com
*/

#include "KatanaEngine.h"

namespace KatanaEngine
{
	bool Audio::s_alAddonInitialized = false;

	bool Audio::Load(const std::string& path, ResourceManager* pManager)
	{
		if (!s_alAddonInitialized)
		{
			// Install the audio subsystem
			al_install_audio();
			// Initialize the codec
			al_init_acodec_addon();
			s_alAddonInitialized = true;
		}

		// Reserve a layer for the audio samples
		al_reserve_samples(1);

		// Load the path for the sample
		m_psample = al_load_sample(path.c_str());
		return (m_psample != nullptr);
	}

	void Audio::PlaySampleInstance(ALLEGRO_PLAYMODE mode) {

		// Create an instance of the sample
		m_psampleInstance = al_create_sample_instance(m_psample);

		// Set the instance playmode. Currently set to once, but can pass ALLEGRO_PLAYMODE_LOOP
		al_set_sample_instance_playmode(m_psampleInstance, mode);

		// Attach the instance to the default mixer
		al_attach_sample_instance_to_mixer(m_psampleInstance, al_get_default_mixer());

		// Play the instance
		al_play_sample_instance(m_psampleInstance);

	}
	void Audio::StopSampleInstance() {
		al_stop_sample_instance(m_psampleInstance);
	}

}